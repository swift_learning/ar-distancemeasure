//
//  ViewController.swift
//  AR DistanceMeasure
//
//  Created by iulian david on 8/14/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController {
    
    @IBOutlet var sceneView: ARSCNView!
    
    var dotNodes = [SCNNode]()
    var textNode = SCNNode()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show the points
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if dotNodes.count >= 2 {
            dotNodes.forEach({ (dotNode) in
                dotNode.removeFromParentNode()
            })
            textNode.removeFromParentNode()
            dotNodes = [SCNNode]()
        }
        guard let touchLocation = touches.first?.location(in: sceneView) else {return}
        
        let hittestResults = sceneView.hitTest(touchLocation, types: .featurePoint)
        
        guard let hitResult = hittestResults.first else {return}
        
        addDot(at: hitResult)
        
    }
    
    func addDot(at hitResult: ARHitTestResult) {
        let dotGeometry = SCNSphere()
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red
        
        dotGeometry.materials = [material]
        dotGeometry.radius = 0.005
        
        let dotNode = SCNNode(geometry: dotGeometry)
        
        dotNode.position = SCNVector3(
            x: hitResult.worldTransform.columns.3.x,
            y: hitResult.worldTransform.columns.3.y + 0.005,
            z: hitResult.worldTransform.columns.3.z
        )
        sceneView.scene.rootNode.addChildNode(dotNode)
        
        dotNodes.append(dotNode)
        
        if dotNodes.count >= 2 {
            calculate()
        }
        
    }
    
    func calculate() {
        let start = dotNodes[0]
        let end = dotNodes[1]
        
        //distance = √((x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2)
        //distance = √((a)^2 + (b)^2 + (c)^2)
        
        let a = end.position.x - start.position.x
        let b = end.position.y - start.position.y
        let c = end.position.z - start.position.z
        
        let distance = sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2))
        
        updateText(text: "\(abs(distance))", atPosition: end.position)
    }
    
    
    func updateText(text: String, atPosition position: SCNVector3) {
        
        let textGeometry = SCNText(string: text, extrusionDepth: 1.0)
        
        textGeometry.firstMaterial?.diffuse.contents = UIColor.red
        
        textNode = SCNNode(geometry: textGeometry)
        
        textNode.position = SCNVector3(position.x, position.y + 0.01, position.z)
        textNode.scale = SCNVector3(0.01, 0.01, 0.01)
        
        sceneView.scene.rootNode.addChildNode(textNode)
        
    }
}

// MARK: - ARSCNViewDelegate Methods
extension ViewController: ARSCNViewDelegate {
    
}
